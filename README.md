# Samsung Software Membership Mini project #

## 핸드폰 자이로 센서를 이용한 리듬게임 ##
* Unity   : 실제 게임에 쓰임 (작곡, Play기능)
* Android : 안드로이드 디바이스 내의 음악을 찾아 리스트로 보여줌

## 개발 목표 ##
* 자이로 센서를 이용한 흔드는 동작으로 게임을 진행, 편하게 리듬을 작곡하여 누구나 자신을 리듬을 공유할수 있는 시스템 구축

## 시스템 아키텍쳐 ##
![actbit아키텍쳐.PNG](https://bitbucket.org/repo/eM494b/images/3723322518-actbit%EC%95%84%ED%82%A4%ED%85%8D%EC%B3%90.PNG)

## Rhythm Data Database 저장 예시 ##
* 파일명 : 가수이름_곡이름.TXT
* 저장 방식 : 리듬 시간 정보 / 방향 정보(0 : 좌, 1 : 우, 2 : 상, 3 : 하)
* 예시 ) 0.533/0/2.005/1...

## Android Media Scan을 통한 기기 내부 음악 검색Flow Chart ##
* ![flowchart.PNG](https://bitbucket.org/repo/eM494b/images/362236903-flowchart.PNG)

## Android / Unity 연동 ##
Android(해당 씬 호출) --(리듬 게임 : Mp3파일경로, 난이도, 노트속도, 진동유무)--> 유니티 게임 Scene 
유니티(Activity 호출) --(노트 작곡 : Mp3파일경로, 리듬파일 이름 )-- Android Rhythm Upload to server

## 작곡 모드 예시##
* ![작곡.PNG](https://bitbucket.org/repo/eM494b/images/2192041-%EC%9E%91%EA%B3%A1.PNG)
* (좌) 노래가 나오며 알맞는 리듬에 맞춰 작곡
* (우) 리듬 데이터 세밀 조정

## 센서 판별 구간 예시 ##
* ![센서.PNG](https://bitbucket.org/repo/eM494b/images/1005340131-%EC%84%BC%EC%84%9C.PNG)

## 노트 속도 예시 ##
* ![노트.PNG](https://bitbucket.org/repo/eM494b/images/3333506885-%EB%85%B8%ED%8A%B8.PNG)

## 사용자 시나리오 ##
* 1. Splash Logo, Google 계정 연동
* ![story1.PNG](https://bitbucket.org/repo/eM494b/images/4221709609-story1.PNG)
* 2. 최초 접속시 튜토리얼 진행
* ![story2.PNG](https://bitbucket.org/repo/eM494b/images/2909469291-story2.PNG)
* 3. Main화면, Option 화면
* ![story3.PNG](https://bitbucket.org/repo/eM494b/images/1454005895-story3.PNG)
* 4. 곡 선택 화면, 난이도 조절 화면
* ![story4.PNG](https://bitbucket.org/repo/eM494b/images/2315624350-story4.PNG)
* 5. 게임 Play 화면, 점수 화면
* ![story5.PNG](https://bitbucket.org/repo/eM494b/images/1931405407-story5.PNG)
