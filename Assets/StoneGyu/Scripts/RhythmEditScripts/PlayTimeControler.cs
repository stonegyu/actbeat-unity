﻿using UnityEngine;
using System.Collections;

public class PlayTimeControler : MonoBehaviour {
	public float hSbarValue;
	float TotalMusicTime;
	public AudioSource audiosource;
	bool isPlay = false;
	// Use this for initialization
	WWW www;
	AudioClip clip;

	public void getmp3root(string path){
		www = new WWW ("file://"+path);
		while(!www.isDone){
		}
		clip = www.GetAudioClip(false);
		audiosource.clip = clip;
		audiosource.playOnAwake = true;
		TotalMusicTime = audiosource.clip.length;
	}
	public void SetTotalMusicTime(float TotalMusicTime){
		this.TotalMusicTime = TotalMusicTime;
	}
	void Start () {
		Screen.SetResolution (480, 800, true);
		Screen.orientation = ScreenOrientation.Portrait;

	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.Find("DecibelCamera").transform.position.x>=0f && !audiosource.isPlaying){
			if(!isPlay){
				Camera.main.transform.GetComponent<PlayTimeControler>().audiosource.playOnAwake = true;
				Camera.main.transform.GetComponent<PlayTimeControler>().audiosource.Play();
				isPlay=true;
			}
			
		}

		if(GameObject.Find("DecibelCamera").transform.GetComponent<RhythmEditScript>().EditRhythmMode){
			if(fabs(audiosource.time - hSbarValue)>0.250f){
				if(audiosource.time<= TotalMusicTime)
					audiosource.time = hSbarValue;
			}
		}
		hSbarValue = audiosource.time;

	}
	public void UpdateAudioTime(float Update_Time){
		hSbarValue = Update_Time;
		audiosource.time = Update_Time;

	}
	public void UpdateAudioPlay(){
		isPlay = false;
	}
	public void UpdateAudioStop(){
		audiosource.Stop ();
	}
	void OnGUI(){
		if(GameObject.Find("DecibelCamera").transform.GetComponent<RhythmEditScript>().EditRhythmMode){
			hSbarValue = GUI.HorizontalScrollbar (new Rect (0, Screen.height * 0.45f, Screen.width, Screen.height * 0.1f), hSbarValue,1 , 0f, TotalMusicTime);
			return;
		}
		hSbarValue = GUI.HorizontalScrollbar (new Rect (0, Screen.height * 0.21f, Screen.width, Screen.height * 0.1f), hSbarValue,1 , 0f, TotalMusicTime);
	}
	float fabs(float time){
		if(time<0) return time*-1;
		return time;
	}
}
