﻿using UnityEngine;
using System.Collections;

public class IndicatorSetup : MonoBehaviour {

	public GameObject[] elementPrefab = new GameObject[4];
	// Use this for initialization
	void Start () {
		int bands = GetComponent<AudioAnalyzer> ().numberOfBands;
		for(int i=0; i<bands; i++){
			elementPrefab[i].GetComponent<IndicatorElement>().index = i;
			elementPrefab[i].name = "Decible "+i;
			elementPrefab[i].transform.parent = transform;
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
