﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using LitJson;
public class Server_SendReceive : MonoBehaviour{
	List<RhythmData> RhythmList = new List<RhythmData>();
	public string url_Path = "";
	Server_SendReceive(){}
	private WWW GET(string url){
		WWW www = new WWW(url);
		while(true){
			if(www.isDone==true){
				break;
			}
		}
		return www;
	}
	public int LoadServer_RankData(string file_name){
		string url = "http://stonegyu.cafe24.com/ActBit/Load_MusicRank.php";
		WWWForm form = new WWWForm ();
		form.AddField ("music_filename", file_name);
		WWW www = new WWW(url, form);
		while(true){
			if(www.isDone == true) {
				break;
			}
		}

		if(www.text.Equals("null") || www.text.Length == 0) {
			return 0;
		}
		LitJson.JsonData getData = LitJson.JsonMapper.ToObject(www.text);
		string data = getData[0]["Score"].ToString();
		print("data : "+data);
		if(data.Equals("NODATA")){
			data = "0";
		}
		return int.Parse(data);
	}
	public void UpdateServer_BestRhythmer(string file_name, string userID, int score){

		string url = "http://stonegyu.cafe24.com/ActBit/Update_BestRhytmer.php";
		WWWForm form = new WWWForm ();
		form.AddField ("music_filename", file_name);
		form.AddField ("bestrhythmer", userID);
		form.AddField ("score", score);
		WWW www = new WWW(url, form);
		while(true){
			if(www.isDone == true) {
				break;
			}
		}
		print (www.text);
	}
	public void SaveServer_RhythmData(string RhythmData, string file_name){
		string url = "http://stonegyu.cafe24.com/ActBit/Save_MusicNote.php";
		WWWForm form = new WWWForm ();
		form.AddField ("music_filename", file_name);
		form.AddField ("music_notedata", RhythmData);

		WWW www = new WWW (url, form);
		while(true){
			if(www.isDone == true) {
				break;
			}
		}
	}
	RhythmData temp = new RhythmData();
	public List<RhythmData> LoadServer_RhythmData(string file_name){
		List<RhythmData> RhythmList = new List<RhythmData>();
		byte[] defaultBytes = Encoding.Default.GetBytes("http://stonegyu.cafe24.com/ActBit/Load_MusicNote.php/?music_filename="+file_name);  
		byte[] utf8Bytes = Encoding.Convert( Encoding.Default, Encoding.UTF8, defaultBytes );  
		string url = Encoding.UTF8.GetString( utf8Bytes ); 
		WWW www = GET (url);
		if(www.text.Equals("null") || www.text.Length == 0) {
			RhythmList[0].RhythmTime = -1;
			return RhythmList;
		}
		LitJson.JsonData getData = LitJson.JsonMapper.ToObject(www.text);
		string data = getData[0]["music_notedata"].ToString();
		string[] split_data = data.Split('\\');


		for(int i=0; i<split_data.Length-1; i++){

			if(i%2==0){

				temp.RhythmTime=float.Parse(split_data[i].ToString());
				continue;
			}
			temp.Defection=int.Parse(split_data[i].ToString());
			RhythmList.Add(new RhythmData(temp.RhythmTime, temp.Defection, 0));

		}
		return RhythmList;
	}
}
