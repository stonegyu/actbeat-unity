﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class AudioAnalyzer : MonoBehaviour {
	public float interval = 0.02f;
	public int spectrumSamples = 1024;
	public int numberOfBands = 8;
	public AudioSource audioSource;
	float[] rawSpectrum; //스펙트럼 샘플링 수
	public float[] bandLevels;
	public float[] samples;
	public float dbValue;
	// Use this for initialization
	IEnumerator Start() {
		rawSpectrum = new float[spectrumSamples];
		samples = new float[spectrumSamples];
		bandLevels = new float[numberOfBands];
		print ("" + Camera.main.GetComponent<AudioSource> ().isPlaying);
		while(true){
			if(Camera.main.GetComponent<Receive_RhythmData>().audiosource.isPlaying){
				AudioListener.GetSpectrumData(rawSpectrum, 0, FFTWindow.Hanning);
				ConvertRawSpectrumToBandLevels();
				yield return new WaitForSeconds(interval);
			}else{
				yield return new WaitForSeconds(0.05f);
			}
		}
	}
	void ConvertRawSpectrumToBandLevels(){
		float coeff = Mathf.Log (rawSpectrum.Length);
		int offs = 0;
		for(int i=0; i<bandLevels.Length; i++){
			float next = Mathf.Exp (coeff/ bandLevels.Length * (i+1));
			float weight = 1.0f / (next - offs);
			float sum;
			for(sum = 0.0f; offs < next; offs++){
				sum+=rawSpectrum[offs];
			}
			if(i == bandLevels.Length-1 || i == bandLevels.Length-6 || i == bandLevels.Length-9 || i == bandLevels.Length-11){
				bandLevels[bandLevels.Length-1-i] = bandLevels[i];
			}
			if(Mathf.Sqrt (weight*sum)==0){
				bandLevels[i] = bandLevels[bandLevels.Length-i];
			}else{
				bandLevels[i] = Mathf.Sqrt (weight*sum);
			}
		}

	}
	// Update is called once per frame
	void Update () {
	
	}
}
