﻿using UnityEngine;
using System.Collections;

public class IndicatorElement : MonoBehaviour {
	public int index = 0;
	private float meter = 0.0f;
	static private AudioAnalyzer analyzer;
	public Material[] bitMaterial = new Material[3];
	bool isStart = false;
	// Use this for initialization
	void Start () {
		if(!analyzer) analyzer = (AudioAnalyzer)FindObjectOfType(typeof(AudioAnalyzer));
		isStart=true;
	}
	// Update is called once per frame
	void Update () {
		if(isStart){
			meter = Mathf.Max (meter * Mathf.Exp (-10.0f * Time.deltaTime), analyzer.bandLevels [index]);
			float temp = meter * 8.0f;
			print (temp);
			if(temp>0.8){
				gameObject.renderer.material = bitMaterial[0];
			}else if(temp<0.8 && temp>0.5){
				gameObject.renderer.material = bitMaterial[1];
			}else if(temp<0.5){
				gameObject.renderer.material = bitMaterial[2];
			}
			transform.localScale = new Vector3(temp, temp, temp);
		}
	}
}
