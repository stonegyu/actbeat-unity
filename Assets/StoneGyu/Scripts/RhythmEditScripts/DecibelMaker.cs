﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
public class DecibelMaker : MonoBehaviour {
	public class DecibelInfo{
		public float Decibel_Time;
		public float Decibel_Value;
		public Vector3 Decibel_Position;
		public DecibelInfo(float Decibel_Time, float Decibel_Value, Vector3 Decibel_Position){
			this.Decibel_Time = Decibel_Time;
			this.Decibel_Value = Decibel_Value;
			this.Decibel_Position = Decibel_Position;
		}
	}
	public float interval = 10000f;
	public int spectrumSamples = 1024;
	public GameObject Decibel_Obj;

	public float[] samples;
	public float dbValue;
	public float Decibel_StartPosition = 0f,Decibel_EndPosition=0, DecibelDelta_D, AudioDelta_T=0,AudioStart_T=0, AudioTotal_T=0;
	public AudioSource DecibelSource;
	GameObject[] Decibel_temp;
	// Use this for initialization
	int Decibel_obj_Count=0;
	float Decibel_i = 0;

	WWW www;
	public AudioClip clip=null;
	public void CallAndroidFunction(string str){
		AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
		jo.Call ("AndroidFunction", str);
	}
	public void getmp3root(string path){
		www = new WWW ("file://"+path);
		while(!www.isDone){
		}
		clip = www.GetAudioClip(false);

		DecibelSource.clip = clip;
		DecibelSource.playOnAwake = true;
	}
	public float CurrentPosition_To_Time(Vector3 CurrentPosition){
		float CurrentDistance = CurrentPosition.x-Decibel_StartPosition;
		return CurrentDistance*AudioTotal_T/Decibel_EndPosition;
	}

	public List <DecibelInfo>Decibel_List = new List<DecibelInfo>();
	IEnumerator Start() {
		DecibelDelta_D=0.2f;
		Decibel_temp = new GameObject[80];
		for(int i=0; i<Decibel_temp.Length; i++){
			Decibel_temp[i] = (GameObject)Instantiate (Decibel_Obj);
			Decibel_temp[i].transform.position = new Vector3 (0, 0, 0);
			Decibel_temp[i].transform.localScale = new Vector3 (0f, 0f, 0);
			Decibel_temp[i].transform.parent = GameObject.Find("DecibelRoot").transform;
		}
		CallAndroidFunction("load_root");
		while (true) {
			if(DecibelSource.clip == null){
				yield return DecibelSource;
			}else{
				DecibelSource.Play();
				break;
			}
		}

		while(true){
			if(gameObject.transform.GetComponent<RhythmEditScript>().EditRhythmMode && Decibel_EndPosition==0){//초기값이 0일때 한번돌것임
				for(int i=0; i<Decibel_temp.Length; i++){
					Decibel_EndPosition = (Decibel_EndPosition<Decibel_temp[i].transform.position.x)?Decibel_temp[i].transform.position.x:Decibel_EndPosition;
					AudioTotal_T = (AudioTotal_T<Decibel_temp[i].transform.GetComponent<DecibelBarInfo>().Decible_Time)?Decibel_temp[i].transform.GetComponent<DecibelBarInfo>().Decible_Time:AudioTotal_T;
				}
				AudioDelta_T = (AudioTotal_T-AudioStart_T)*DecibelDelta_D/(Decibel_EndPosition-Decibel_StartPosition);

				DecibelSource.Stop();
				break;
			}
			samples = new float[spectrumSamples];
			DecibelSource.GetOutputData(samples,0);
			ConvertDecibelToBandLevels();
			if(!gameObject.GetComponent<AudioSource>().isPlaying && !Camera.main.GetComponent<AudioSource>().isPlaying){
				yield return new WaitForSeconds(interval);
			}
			if(gameObject.GetComponent<RhythmEditScript>().EditRhythmMode){
				if(Camera.main.GetComponent<AudioSource>().isPlaying)
					DecibelCamera_Move();
			}else{
				DecibelCamera_Move();
			}
			yield return new WaitForSeconds(interval);
		}
	}
	int CurrentDecibel_MaxAddress = 79;
	int CurrentDecibel_MinAddress = 0;
	void Update(){
		float min_distance = 100000, max_distance = 0;
		int min_location = 0, max_location = 0;
		if(!DecibelSource.isPlaying){
			for(int i=0; i<Decibel_temp.Length; i++){
				if(min_distance>Decibel_temp[i].transform.position.x){
					min_distance = Decibel_temp[i].transform.position.x;
					min_location = i;
				}
				if(max_distance<Decibel_temp[i].transform.position.x){
					max_distance = Decibel_temp[i].transform.position.x;
					max_location = i;
				}
			}
			float min = gameObject.transform.position.x-Decibel_temp[min_location].transform.position.x;
			float max = Decibel_temp[max_location].transform.position.x-gameObject.transform.position.x;
			if(min <21f&& CurrentDecibel_MinAddress-1>0){ // 되감기 

				CurrentDecibel_MinAddress--;
				CurrentDecibel_MaxAddress--;
				Decibel_temp[max_location].transform.position = new Vector3(5f*Decibel_List[CurrentDecibel_MinAddress].Decibel_Time, 0, 0);
				Decibel_temp[max_location].transform.localScale = new Vector3(0.025f, 0.1f, Decibel_List[CurrentDecibel_MinAddress].Decibel_Value);
			}
			if(max < 21f && CurrentDecibel_MaxAddress+1<Decibel_List.Count){ //빨리감기
				CurrentDecibel_MinAddress++;
				CurrentDecibel_MaxAddress++;
				Decibel_temp[min_location].transform.position = new Vector3(5f*Decibel_List[CurrentDecibel_MaxAddress].Decibel_Time, 0, 0);
				Decibel_temp[min_location].transform.localScale = new Vector3(0.025f, 0.1f, Decibel_List[CurrentDecibel_MaxAddress].Decibel_Value);
			}
		}
		if(gameObject.transform.GetComponent<RhythmEditScript>().EditRhythmMode){
			if(Camera.main.GetComponent<AudioSource>().isPlaying)
				DecibelCamera_Move();

			float max_loc = 0;
			int max_i=0;
			float min_loc = 100000;
			int min_i=0;
			for(int i=0; i<Decibel_temp.Length; i++){
				
				if(min_loc>Decibel_temp[i].transform.position.x){
					min_loc = Decibel_temp[i].transform.position.x;
					min_i = i;
				}
				if(max_loc<Decibel_temp[i].transform.position.x){
					max_loc = Decibel_temp[i].transform.position.x;
					max_i = i;
				}
			}
			if(gameObject.transform.position.x > max_loc){
				gameObject.transform.position = new Vector3(Decibel_temp[max_i].transform.position.x, transform.position.y, transform.position.z);
				Camera.main.GetComponent<RhythmMaker>().audiosource.Stop();
			}
			if(gameObject.transform.position.x<min_loc){
				gameObject.transform.position = new Vector3(Decibel_temp[min_i].transform.position.x, transform.position.y, transform.position.z);
			}
		}

	}
	public void EditModeDecibelMake(){
		for(int i=0; i<Decibel_temp.Length; i++){
			Decibel_temp[i].GetComponent<DecibelBarInfo>().Decible_Time = Decibel_List[i].Decibel_Time;
			Decibel_temp[i].transform.position = new Vector3 (5f*Decibel_List[i].Decibel_Time, 0, 0);
			Decibel_temp[i].transform.localScale = new Vector3 (0.025f, 0.1f, Decibel_List[i].Decibel_Value);
		}
	}
	void DecibelCamera_Move(){
		if(!Camera.main.transform.GetComponent<PlayTimeControler>().audiosource.isPlaying){
			transform.position = new Vector3(-20f+5f*DecibelSource.time, transform.position.y,transform.position.z);
			return;
		}
		transform.position = new Vector3(5f*Camera.main.transform.GetComponent<PlayTimeControler>().audiosource.time, transform.position.y,transform.position.z);

	}
	bool isFirst = false;
	void ConvertDecibelToBandLevels(){
		float sum = 0;
		for(int i=0; i<samples.Length; i++){
			sum += samples[i]*samples[i];
		}
		float rmsValue = Mathf.Sqrt (sum / samples.Length);
		dbValue = 20 * Mathf.Log10 (rmsValue / 0.1f); //0.1f = refvalue
		if(dbValue < -160) dbValue = 0;
		if(!isFirst) {
			AudioStart_T = DecibelSource.time;
			isFirst=true;
		}
		Decibel_List.Add (new DecibelInfo(DecibelSource.time, dbValue*0.05f,new Vector3 (5f*DecibelSource.time, 0, 0)));
		Decibel_temp[Decibel_obj_Count%80].GetComponent<DecibelBarInfo>().Decible_Time = DecibelSource.time;
		Decibel_temp[Decibel_obj_Count%80].transform.position = new Vector3 (5f*DecibelSource.time, 0, 0);
		Decibel_temp[Decibel_obj_Count%80].transform.localScale = new Vector3 (0.025f, 0.1f, Decibel_List[Decibel_List.Count-1].Decibel_Value);
		Decibel_temp[Decibel_obj_Count++%80].transform.tag = "DecibelBar";
	}
}
