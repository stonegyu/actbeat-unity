﻿using UnityEngine;
using System.Collections;

public class AudioTimeController : MonoBehaviour {
	Vector2 mousePos;
	float startPoint;
	float endPoint;
	float RhythmBeforePoint;
	float RhythmAfterPoint;
	public bool IsDrag = false;
	bool IsRhythmDrag =false;

	bool isRhythmDataSelected = false;
	bool isDerectionChanged = false;
	bool isRhythmDataMaked = false;
	bool isStartTimeMode = true;
	bool isDataChanged = false;

	Transform[] RhythmDataList;
	int SelectedRhythm=-1;
	public const int LEFT = 0, RIGHT = 1, DOWN = 2, SHAKING = 3;
	public Material[] material = new Material[4]; 
	string[] STR_Direction = {"Left", "Right", "Down", "Shaking"};

	// Use this for initialization
	void Start () {
	
	}
	void OnRhythmObjDrag(){
		RhythmAfterPoint = Input.mousePosition.x;
		if(RhythmBeforePoint<RhythmAfterPoint){
			SelectedRhythmData.transform.position += new Vector3(-Screen.width*0.000007f*(RhythmBeforePoint-RhythmAfterPoint), 0, 0);
		}else{
			SelectedRhythmData.transform.position += new Vector3(Screen.width*0.000007f*(RhythmAfterPoint-RhythmBeforePoint), 0, 0);
		}

		SelectedRhythmData.Rhythm_Time=gameObject.transform.GetComponent<DecibelMaker>().CurrentPosition_To_Time(SelectedRhythmData.transform.position);
		Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key,0,
		                                                                  SelectedRhythmData.Rhythm_Time, 
		                                                                  SelectedRhythmData.Rhythm_Direction);
		//쉐이킹 모드 일때랑 나눠야함

		RhythmBeforePoint = RhythmAfterPoint;
	}
	// Update is called once per frame
	void OnMouseDrag() {
		if(isRhythmDataSelected){
			return;
		}
		endPoint = Input.mousePosition.x; 
		if(startPoint <endPoint){
			gameObject.transform.position += new Vector3(-Screen.width*0.000001f*(endPoint-startPoint), 0, 0);
		}else{
			gameObject.transform.position += new Vector3(Screen.width*0.000001f*(startPoint-endPoint), 0, 0);
		}

	}
	void GUI_EditTool(){
		float Delta_T = gameObject.transform.GetComponent<DecibelMaker>().AudioDelta_T;
		float Delta_D = gameObject.transform.GetComponent<DecibelMaker>().DecibelDelta_D;
		Screen_Height+=ScreenHeightPlus;
		int frontOrRear=0; //front 0 rear 1
		if(GUI.Button (new Rect (0, Screen_Height, Screen_Width ,Screen_Height/4), "+")){
			if(SelectedRhythmData.Rhythm_Direction == SHAKING){

				if(!isStartTimeMode){
					SelectedRhythmData.Rhythm_EndTime += Delta_T;
					SelectedRhythmData.transform.localScale += new Vector3(Delta_D, 0, 0);
					frontOrRear=1;
					Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key,frontOrRear,
					                                                                  SelectedRhythmData.Rhythm_EndTime, 
					                                                                  SelectedRhythmData.Rhythm_Direction);
				}else{
					if(SelectedRhythmData.Rhythm_EndTime<=SelectedRhythmData.Rhythm_Time+0.2f) return; //예외상황 start_time이 end_time보다 클때
					SelectedRhythmData.Rhythm_Time+=Delta_T;
					SelectedRhythmData.transform.localScale -= new Vector3(Delta_D, 0, 0);
					frontOrRear=0;
					Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key,frontOrRear,
					                                                                  SelectedRhythmData.Rhythm_Time, 
					                                                                  SelectedRhythmData.Rhythm_Direction);
				}
				SelectedRhythmData.transform.position += new Vector3(Delta_D/2f, 0, 0);
			}else{
				SelectedRhythmData.Rhythm_Time+=Delta_T;
				SelectedRhythmData.transform.position += new Vector3(Delta_D, 0, 0);
				Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key,frontOrRear,
				                                                                  SelectedRhythmData.Rhythm_Time, 
				                                                                  SelectedRhythmData.Rhythm_Direction);
			}
		}
		frontOrRear=0;
		if(GUI.Button (new Rect (Screen_Width, Screen_Height, Screen_Width,Screen_Height/4), "-")){

			if(SelectedRhythmData.Rhythm_Direction == SHAKING){
				if(!isStartTimeMode){
					if(SelectedRhythmData.Rhythm_EndTime<=SelectedRhythmData.Rhythm_Time+0.2f) return; //예외상황 start_time이 end_time보다 작을떄
					SelectedRhythmData.Rhythm_EndTime -= Delta_T;
					SelectedRhythmData.transform.localScale -= new Vector3(Delta_D, 0, 0);
					frontOrRear=1;
					Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key, frontOrRear,
					                                                                  SelectedRhythmData.Rhythm_EndTime, 
					                                                                  SelectedRhythmData.Rhythm_Direction);
				}else{
					SelectedRhythmData.Rhythm_Time-= Delta_T;
					SelectedRhythmData.transform.localScale += new Vector3(Delta_D, 0, 0);
					frontOrRear=0;
					Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key, frontOrRear,
					                                                                  SelectedRhythmData.Rhythm_Time, 
					                                                                  SelectedRhythmData.Rhythm_Direction);
				}
				SelectedRhythmData.transform.position -= new Vector3(Delta_D/2f, 0, 0);
			}else{
				SelectedRhythmData.Rhythm_Time-= Delta_T;
				SelectedRhythmData.transform.position -= new Vector3(Delta_D, 0, 0);
				Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key, frontOrRear,
				                                                                  SelectedRhythmData.Rhythm_Time, 
				                                                                  SelectedRhythmData.Rhythm_Direction);
			}
			
		}
		if(GUI.Button (new Rect (Screen_Width*2, Screen_Height, Screen_Width,Screen_Height/4), 
		               "startTime\n"+SelectedRhythmData.Rhythm_Time)){
			isStartTimeMode = true;
		}
		if(GUI.Button (new Rect (Screen_Width*3, Screen_Height, Screen_Width,Screen_Height/4)
		               , "EndTime"+SelectedRhythmData.Rhythm_EndTime)){
			isStartTimeMode = false;
		}
		if(GUI.Button (new Rect (Screen_Width*4, Screen_Height, Screen_Width,Screen_Height/4), "Delete")){
			Destroy(RhythmDataList[SelectedRhythm].gameObject);
			SelectedRhythm=-1;
			isRhythmDataSelected = false;
			
		}
	}
	public RhythmDataManager SelectedRhythmData;
	void GUI_RhythmPlusMinus(){
		if(isRhythmDataSelected){
			isRhythmDataMaked = false;
			
			Screen_Height+=ScreenHeightPlus;
			if(SelectedRhythmData.Rhythm_Direction == SHAKING){
				GUI_EditTool();
				return;
			}
			if(GUI.Button (new Rect (0, Screen_Height, Screen_Width ,Screen_Height/4), "Closed\n"+STR_Direction[SelectedRhythmData.Rhythm_Direction])){
				isRhythmDataSelected = false;
			}
			
			for(int i=0,j=1; i<STR_Direction.Length-1; i++){
				if(i == SelectedRhythmData.Rhythm_Direction)continue;
				if(GUI.Button (new Rect(Screen_Width*j++, Screen_Height, Screen_Width, Screen_Height/4),""+STR_Direction[i])){
					
					Camera.main.transform.GetComponent<RhythmMaker>().Edit_RhythmData(SelectedRhythmData.Key, 0,
					                                                                  SelectedRhythmData.Rhythm_Time, 
					                                                                  i);
					SelectedRhythmData.gameObject.renderer.material = material[i];
					SelectedRhythmData.Rhythm_Direction = i;
					isDerectionChanged = false;
					break;
					
				}
			}
			GUI_EditTool();
			
		}
	}
	void CallAndroidFunction(string str){
		AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
		jo.Call ("AndroidFunction", str);
	}

	public string filename="";
	public void AndroidGet_NoteFileName(string file_name){
		filename = file_name;
	}
	float Screen_Width;
	float Screen_Height;
	float ScreenHeightPlus;
	public GUISkin sk;
	void OnGUI(){

		if(gameObject.GetComponent<RhythmEditScript>().EditRhythmMode){
			Screen_Width = Screen.width*0.2f;
			Screen_Height = Screen.height/2;
			ScreenHeightPlus = Screen_Height/3;
			GUI.skin = sk;

			if(GUI.Button (new Rect (0, Screen_Height, Screen_Width,Screen_Height/3), "Play")){
				Camera.main.transform.GetComponent<PlayTimeControler>().UpdateAudioPlay();
			}
			if(GUI.Button (new Rect (Screen_Width, Screen_Height, Screen_Width,Screen_Height/3), "Pause")){
				Camera.main.transform.GetComponent<PlayTimeControler>().UpdateAudioStop();
			}
			if(GUI.Button (new Rect (Screen_Width*2, Screen_Height, Screen_Width,Screen_Height/3), "Make")){
				isRhythmDataMaked = !isRhythmDataMaked;
			}
			if(GUI.Button (new Rect (Screen_Width*3, Screen_Height, Screen_Width,Screen_Height/3), "Save")){
				string rhythmdata = Camera.main.GetComponent<RhythmMaker>().RhythmData_ToString();
				gameObject.transform.GetComponent<Server_SendReceive>().SaveServer_RhythmData(rhythmdata,filename);
				Camera.main.GetComponent<RhythmMaker>().SaveRhythmData(filename);
			}
			if(GUI.Button (new Rect (Screen_Width*4, Screen_Height, Screen_Width,Screen_Height/3), "Exit")){
				CallAndroidFunction("call_musicList");
				Application.Quit();
			}
			if(isRhythmDataMaked){
				isRhythmDataSelected = false;
				Screen_Height+=ScreenHeightPlus;
				for(int i=0, j=1; i<STR_Direction.Length; i++){
					if(GUI.Button (new Rect(Screen_Width*j++, Screen_Height, Screen_Width, Screen_Height/4),""+STR_Direction[i])){
						if(i == 3){
							Camera.main.GetComponent<RhythmMaker>().MakeRhythm(i, false);
							Camera.main.GetComponent<RhythmMaker>().MakeRhythm(i, true);
						}else{
							Camera.main.GetComponent<RhythmMaker>().MakeRhythm(i, false);
						}

						break;
					}
				}
			}
			GUI_RhythmPlusMinus();
		}
	}
	void Update () {

		if(gameObject.GetComponent<RhythmEditScript>().EditRhythmMode){		
			if(IsDrag){
				OnMouseDrag();
			}
			if(IsRhythmDrag){
				OnRhythmObjDrag();
			}
			if (Input.GetMouseButtonDown(0)) {
				if(Input.mousePosition.y >Screen.height*0.56f){
					Camera.main.transform.GetComponent<PlayTimeControler>().UpdateAudioStop();

					Ray ray = gameObject.camera.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;
					if(Physics.Raycast(ray, out hit)){
						RhythmDataList = GameObject.Find("RhythmDatas").GetComponentsInChildren<Transform>();
						for(int i=0; i<RhythmDataList.Length; i++){	
							if(hit.collider.name.Equals(RhythmDataList[i].name)){
								SelectedRhythm = i;
								SelectedRhythmData = RhythmDataList[SelectedRhythm].GetComponent<RhythmDataManager>();
								isRhythmDataSelected = true;
								IsRhythmDrag = true;
								RhythmBeforePoint = Input.mousePosition.x;
								return;
							}
						}

					}
					isRhythmDataSelected=false;
					IsDrag=true;
					startPoint=Input.mousePosition.x;
				}
			}
			if (Input.GetMouseButtonUp(0) && IsRhythmDrag) {
				IsRhythmDrag = false;
				RhythmBeforePoint=0;
				RhythmAfterPoint = 0;
			}
			if (Input.GetMouseButtonUp(0) && IsDrag) {
				IsDrag=false;

			}

		}
	}
}
