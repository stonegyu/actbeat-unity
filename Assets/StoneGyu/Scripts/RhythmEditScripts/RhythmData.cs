﻿using UnityEngine;
using System.Collections;

public class RhythmData{
	public int LEFT = 0, RIGHT = 1, DOWN = 2, SHAKING = 3;
	public float RhythmTime;
	public float RhythmEndTime;
	public int Defection;
	public int key;
	public RhythmData(){
		this.RhythmTime = 0;
		this.Defection = 0;
		this.key=0;
	}
	public RhythmData(float RhythmTime, int Derection, int key){
		this.RhythmTime = RhythmTime;
		this.Defection = Derection;
		this.key=key;
	}
	public void Clear(){
		RhythmTime = 0;
		Defection = 0;
		key = 0;
	}
}
