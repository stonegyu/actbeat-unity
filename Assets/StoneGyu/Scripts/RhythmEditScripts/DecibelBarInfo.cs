﻿using UnityEngine;
using System.Collections;

public class DecibelBarInfo : MonoBehaviour {
	public float Decible_Value;
	public float Decible_Time;
	public int INDEX;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(!Camera.main.transform.GetComponent<PlayTimeControler>().audiosource.isPlaying){

			if(Vector3.Distance(gameObject.transform.position, GameObject.Find ("AudioTimeBar").transform.position)<0.2f){
				Camera.main.transform.GetComponent<PlayTimeControler>().UpdateAudioTime(Decible_Time);

			}
		}
	}
}
