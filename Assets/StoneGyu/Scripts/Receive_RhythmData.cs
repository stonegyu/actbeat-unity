﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Receive_RhythmData : MonoBehaviour {

	List <RhythmData> RhythmList = new List<RhythmData>();
	public AudioSource audiosource;
	Create_Rhythm Creater;
	int ShakingFlag=0;
	// Use this for initialization
	int CurrentRhythmIndex = 0;
	float playWaitTime = 0;
	GameObject RhythmData;
	float fallingTime=-1;

	string mp3_fileroot="";
	string music_filename="";
	string rhythm_mode="";
	string User_ID="";
	float endDelayTime = 0;
	bool isEnd  = false;
	int GamePlayMode = 0;
	string viberatemode="";
	public void CallAndroidFunction(string str){
		AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
		jo.Call ("AndroidFunction", str);
	}
	public void Receive_MP3FileRoot_Data(string path){
		WWW www = new WWW ("file://"+path);
		while(!www.isDone){
		}
		print ("error : "+www.error);
		AudioClip clip = www.GetAudioClip(false);
		
		audiosource.clip = clip;
		audiosource.clip.name = path;
		audiosource.playOnAwake = true;
	}
	public void Receive_MusicFilename(string str){
		music_filename = str;
	}
	public void Receive_RhythmMode(string str){
		rhythm_mode = str;
	}
	public void Receive_UserID(string str){
		User_ID = str;
	}
	public void Receive_FallingTime(string str){
		fallingTime = float.Parse(str);
	}
	public float getFallingTime(){
		return fallingTime;
	}
	public void Receive_ViberatorMode(string str){
		viberatemode = str;
	}
	public bool getViberatorMode(){
		if(viberatemode.Equals("on")){
			return true;
		}else{
			return false;
		}

	}
	IEnumerator Start () {
		CallAndroidFunction("gamedata_load");
		while (true) {
			if(audiosource.clip == null){
				yield return audiosource;
			}else{
				print ("name : "+audiosource.clip.name);
				break;
			}
		}
		while (true) {
			if(music_filename.Length == 0){
				yield return new WaitForSeconds(0.01f);
			}else{
				print ("music_filename : "+music_filename);
				break;
			}
		}
		while (true) {
			if(rhythm_mode.Length == 0){
				yield return new WaitForSeconds(0.01f);
			}else{
				break;
			}
		}
		while (true) {
			if(fallingTime == -1){
				yield return new WaitForSeconds(0.01f);
			}else{
				break;
			}
		}
		while (true) {
			if(viberatemode.Length==0){
				yield return new WaitForSeconds(0.01f);
			}else{
				break;
			}
		}
		Screen.sleepTimeout = SleepTimeout.NeverSleep; 

		Creater = GameObject.Find("Rhythm_Creater").transform.GetComponent<Create_Rhythm>();

		Creater.CreateRhythmStart();
		RhythmList = gameObject.transform.GetComponent<SaveAndReadFILE>().readStringFromFile(music_filename);
		for(int i=0; i<RhythmList.Count-1; i++){
			if(RhythmList[i].Defection == RhythmList[i].SHAKING){
				if(ShakingFlag==0){
					ShakingFlag++;
				}else{
					RhythmList[i-1].RhythmEndTime = RhythmList[i].RhythmTime;
					RhythmList.RemoveAt(i--);
					ShakingFlag = 0;
				}
			}
		}
		//fallingTime = 1.0f; //2.0f
		if(rhythm_mode.Equals("hard")){
			GamePlayMode=1;
		}else if(rhythm_mode.Equals("normal")){
			GamePlayMode=2;
		}else if(rhythm_mode.Equals("easy")){
			GamePlayMode=3;
		}
	}

	public int PlayCnt=0;
	void MusicPlay(){
		if(!audiosource.isPlaying){
			PlayCnt++;
			audiosource.playOnAwake = true;
			audiosource.Play();
		}

	}
	// Update is called once per frame
	float DelayTime = 3.0f; //3.0
	float DelayTime2 = -3.0f; //-3.0
	int BestScore = -1;
	bool isPaused = false;
	void Update () {
		if(Application.platform == RuntimePlatform.Android){
			if(Input.GetKeyDown(KeyCode.Escape)){
				isPaused=!isPaused;
				if(isPaused == false){
					audiosource.Play();
					Time.timeScale = 1;
				}else{
					audiosource.Pause();
					Time.timeScale = 0;
				}
			}
		}


		if(playWaitTime>=DelayTime){
			if(PlayCnt==0)
				MusicPlay();
		}else{
			playWaitTime+=Time.deltaTime;
		}
		if(CurrentRhythmIndex < RhythmList.Count){

			if(DelayTime2+playWaitTime+audiosource.time >= RhythmList[CurrentRhythmIndex].RhythmTime-fallingTime){

				if(RhythmList[CurrentRhythmIndex].Defection == RhythmList[CurrentRhythmIndex].SHAKING){

					float duration = RhythmList[CurrentRhythmIndex].RhythmEndTime-RhythmList[CurrentRhythmIndex].RhythmTime;
					Creater.Create_RhythmObj(RhythmList[CurrentRhythmIndex].Defection, true, duration);
					CurrentRhythmIndex+=GamePlayMode;
					return;
				}
				Creater.Create_RhythmObj(RhythmList[CurrentRhythmIndex].Defection, false, 0);
				CurrentRhythmIndex+=GamePlayMode;

			}
			if(Creater.life<=0){
				CurrentRhythmIndex = RhythmList.Count;
			}
		}else{
			if(RhythmList.Count>0){
				if(isEnd){
					endDelayTime+=Time.deltaTime;
					if(endDelayTime<=1.8f){
						audiosource.volume -=  Time.deltaTime/2f;

					}else{
						PlayCnt=2;
						audiosource.Pause();
						isEnd=false;
					}
				}
				if(audiosource.time >= RhythmList[RhythmList.Count-1].RhythmTime || Creater.life<=0 || !audiosource.isPlaying){
					if(audiosource.isPlaying && PlayCnt == 1){
						isEnd = true;
						if(audiosource.timeSamples-RhythmList[RhythmList.Count-1].RhythmTime<=2.0f){
							audiosource.Pause();
						}

					}else if(!audiosource.isPlaying && PlayCnt == 2){
						PlayCnt=3;

					}else if(!audiosource.isPlaying && PlayCnt == 3){
						if(BestScore == -1){
							if(Creater.life>0){
								BestScore = gameObject.transform.GetComponent<Server_SendReceive>().LoadServer_RankData(music_filename);
								print ("Best "+BestScore);
								Create_Rhythm CR = GameObject.Find ("Rhythm_Creater").transform.GetComponent<Create_Rhythm> ();
								int UserScore = GameObject.FindWithTag("GUIManager").GetComponent<GUIController>().PutScore(CR.PerfectCnt, CR.GoodCnt, CR.GreateCnt, CR.BadCnt, CR.MissCnt);

								if(BestScore<UserScore){
									GameObject.FindWithTag("GUIManager").GetComponent<GUIController>().IsBestRhythmer = true;
									gameObject.transform.GetComponent<Server_SendReceive>().UpdateServer_BestRhythmer(music_filename,User_ID,UserScore);
								}
							}else{
								Create_Rhythm CR = GameObject.Find ("Rhythm_Creater").transform.GetComponent<Create_Rhythm> ();
								int UserScore = GameObject.FindWithTag("GUIManager").GetComponent<GUIController>().PutScore(CR.PerfectCnt, CR.GoodCnt, CR.GreateCnt, CR.BadCnt, CR.MissCnt);
							}
						}

					}else if(!audiosource.isPlaying && PlayCnt == 4){
						music_filename = "";
						rhythm_mode="";
						CurrentRhythmIndex = 0;
						playWaitTime=0;
						RhythmList.Clear();
						PlayCnt = 5;
						CallAndroidFunction("call_musicList");
						Application.Quit();
					}
				}
				
			}
		}

	}
	void OnApplicationPause(bool pauseStatus) {
		isPaused = true;
		audiosource.Pause();
		Time.timeScale = 0;

	}
	void OnGUI(){
		if(isPaused == true){
			float ButtonWidth = Screen.width/2;
			float ButtonHeight = Screen.height*0.1f;
			if(GUI.Button(new Rect(Screen.width/2-ButtonWidth/2, Screen.height/2-ButtonHeight/2, ButtonWidth, ButtonHeight),"RESTART")){
				isPaused=false;
				audiosource.Play();
				Time.timeScale = 1;
			}
		}
	}
}
