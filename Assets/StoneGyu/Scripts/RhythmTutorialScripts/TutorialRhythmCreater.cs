﻿using UnityEngine;
using System.Collections;

public class TutorialRhythmCreater : MonoBehaviour {
	public Material[] Rhythm_Material;
	public int TutorialCnt = 0;
	GameObject Recycle_Obj;
	public GameObject Prefabs ;
	Vector3 note_format = new Vector3();
	bool isEffected=true;
	public GameObject[] RhythmDirection_Effect = new GameObject[4];
	int thisDirection=0;
	bool[] ClearNum = new bool[4];
	Gyroscope gyro;
	float Shake_period=0.3f;
	// Use this for initialization
	void Start (){
		gyro = Input.gyro;   
		gyro.enabled = true;
		for(int i=0; i<ClearNum.Length; i++){
			ClearNum[i] = false;
		}
		Recycle_Obj = (GameObject) Instantiate(Prefabs);
		Recycle_Obj.transform.position = new Vector3(0, 1000000,0);
		note_format = Recycle_Obj.transform.localScale;
		float velocity = 9f / 2f;
		Recycle_Obj.rigidbody.velocity = new Vector3(0, -velocity, 0);
	}
	public float fabs(float data)   //절대값
	{
		return (data < 0) ? data * -1 : data;
	}
	// Update is called once per frame
	Vector3 max_vector=Vector3.zero;
	Vector3 min_vector=Vector3.zero;
	bool Shake_flag=false, OkShake_flag=false;
	float time=0;
	void Update () {

		if(TutorialCnt == 1 && ClearNum[0] == false){
			ClearNum[0]=true;
			Create_RhythmObj(0, false, 0);
		}else if(TutorialCnt ==2 && ClearNum[1]==false){
			ClearNum[1]=true;
			Create_RhythmObj(1, false, 0);
		}else if(TutorialCnt == 3 && ClearNum[2]==false){
			ClearNum[2]=true;
			Create_RhythmObj(2, false, 0);
		}else if(TutorialCnt == 4&& ClearNum[3]==false){
			ClearNum[3]=true;
			Create_RhythmObj(3, true, 5f);
		}else{

		}
		if (thisDirection == 3){
			if( fabs( Recycle_Obj.transform.position.y-GameObject.Find("Determining_Area").transform.position.y ) <= Recycle_Obj.transform.localScale.y/2 ) // 바꾸고
			{
				Shake_flag=true;
			}
			else
			{
				Shake_flag=false;//false
			}
		}
		if (Shake_flag){
			//time += Time.deltaTime;
			if(Input.gyro.rotationRate.z>=2f || Input.gyro.rotationRate.z<=-2f || Input.gyro.rotationRate.x<=-2f){
				OkShake_flag=true;
			}
			//
			if(OkShake_flag)
			{
				//scale bozong
				//perfect
				//effect
				Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
				GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(thisDirection+1);
				//Recycle_Obj[CurrentIndex++%Recycle_Obj.Length].gameObject.transform.localScale = new Vector3(temp.x, temp.y+temp.y*(Duration*4.5f), temp.z); 
				Vector3 scale = Recycle_Obj.transform.localScale;
				float d = fabs(Recycle_Obj.transform.position.y-GameObject.Find("Determining_Area").transform.position.y);
				float r = Recycle_Obj.transform.localScale.y;
				float new_r=(d+r/2f);
				if(new_r<=0.5f) {
					Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
					Camera.main.GetComponent<TutorialManager>().MessageCnt=7+4*thisDirection;
					Camera.main.GetComponent<TutorialManager>().Created = true;
					Recycle_Obj.renderer.enabled = false;
					return;
				}
				Recycle_Obj.transform.localScale = new Vector3(scale.x, new_r, scale.z);
				//Recycle_Obj[CurrentIndex%Recycle_Obj.Length].gameObject.transform.position = new Vector3(temp.x,temp.y+(Duration*4.5f)/2f,temp.z);
				Vector3 position = Recycle_Obj.transform.position;
				Recycle_Obj.transform.position = new Vector3(position.x, GameObject.Find("Determining_Area").transform.position.y+new_r/2f,position.z);
				time=0;
			}
			else
			{
				time += Time.deltaTime;
				if(time>Shake_period)
				{
					//miss

					if(Camera.main.GetComponent<TutorialManager>().MessageCnt!=18){
						Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
						Camera.main.GetComponent<TutorialManager>().MessageCnt=6+4*thisDirection-1;
						Camera.main.GetComponent<TutorialManager>().Created = true;
						TutorialCnt--;
						ClearNum[TutorialCnt]=false;
						time=0;
					}

				}
			}
			OkShake_flag=false;
			//
			/*
			if(time>Shake_period)//0.3  
			{
				if(OkShake_flag)//0.3초이내 흔들었다
				{
					Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
					GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(thisDirection+1);
					//Recycle_Obj[CurrentIndex++%Recycle_Obj.Length].gameObject.transform.localScale = new Vector3(temp.x, temp.y+temp.y*(Duration*4.5f), temp.z); 
					Vector3 scale = Recycle_Obj.transform.localScale;
					float d = fabs(Recycle_Obj.transform.position.y-GameObject.Find("Determining_Area").transform.position.y);
					float r = Recycle_Obj.transform.localScale.y;
					float new_r=(d+r/2f);
					if(new_r<=0.1f) {
						Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
						Camera.main.GetComponent<TutorialManager>().MessageCnt=6+4*thisDirection;
						Camera.main.GetComponent<TutorialManager>().Created = true;
						Recycle_Obj.renderer.enabled = false;
					}
					Recycle_Obj.transform.localScale = new Vector3(scale.x, new_r, scale.z);
					//Recycle_Obj[CurrentIndex%Recycle_Obj.Length].gameObject.transform.position = new Vector3(temp.x,temp.y+(Duration*4.5f)/2f,temp.z);
					Vector3 position = Recycle_Obj.transform.position;
					Recycle_Obj.transform.position = new Vector3(position.x, GameObject.Find("Determining_Area").transform.position.y+new_r/2f,position.z);
				}
				else//0.3초이내 안흔들었다
				{
					Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
					Camera.main.GetComponent<TutorialManager>().MessageCnt=6+4*thisDirection-1;
					Camera.main.GetComponent<TutorialManager>().Created = true;
					TutorialCnt--;
					ClearNum[TutorialCnt]=false;
				}
				time=0;
				OkShake_flag=false;


			}
			*/
			return;
		}

		if(fabs(Recycle_Obj.transform.position.y- GameObject.Find("Determining_Area").transform.position.y)<=0.5f){
			bool isCorrect = false;
			if(thisDirection==0){
				max_vector.z = 3f;//최소임계치
				if( max_vector.z < Input.gyro.rotationRate.z )
				{
					isCorrect=true;
				}
			}else if(thisDirection==1){
				min_vector.z = -3f;//최소임계치
				if( min_vector.z > Input.gyro.rotationRate.z )
				{
					isCorrect=true;
				}
			}else if(thisDirection==2){
				min_vector.x = -3f;//최소임계치
				if( min_vector.x > Input.gyro.rotationRate.x )
				{
					isCorrect=true;
				}
			}

			if(!isEffected && isCorrect){
				isEffected=true;
				Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
				GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(thisDirection+1);
				Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
				Camera.main.GetComponent<TutorialManager>().MessageCnt=6+4*thisDirection;
				Camera.main.GetComponent<TutorialManager>().Created = true;
				Recycle_Obj.renderer.enabled = false;
			}
		}
	

		if(Recycle_Obj.transform.position.y<-4){
			Recycle_Obj.transform.position = new Vector3(0, 100000, 0);
			Camera.main.GetComponent<TutorialManager>().MessageCnt=6+4*thisDirection-1;
			Camera.main.GetComponent<TutorialManager>().Created = true;
			TutorialCnt--;
			ClearNum[TutorialCnt]=false;
		}


	}
	Vector3 temp;
	void Create_RhythmObj(int Derection, bool IsShaking, float Duration)
	{
		isEffected=false;
		//노트 생성될떄 , 
		Recycle_Obj.renderer.enabled = true;
		Recycle_Obj.renderer.material = Rhythm_Material[Derection];
		thisDirection = Derection; //방향데이터 임포트 시켜줌
		if(IsShaking)
		{
			Recycle_Obj.gameObject.transform.position = gameObject.transform.position;
			temp = gameObject.transform.position;
			Recycle_Obj.gameObject.transform.position = new Vector3(temp.x,temp.y+(Duration*4.5f)/2f,temp.z);
			temp = note_format;

			Recycle_Obj.gameObject.transform.localScale = new Vector3(temp.x, temp.y+temp.y*(Duration*4.5f), temp.z); 
		}
		else
		{
			Recycle_Obj.gameObject.transform.localScale = note_format;
			Recycle_Obj.gameObject.transform.position = gameObject.transform.position;
		}
		
	}

}
