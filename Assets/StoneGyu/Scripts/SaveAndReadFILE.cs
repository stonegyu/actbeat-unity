﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
public class SaveAndReadFILE : MonoBehaviour {
	public List<RhythmData> RhythmList = new List<RhythmData>();
	public string pathForDocumentsFile(string filename){
		string path = Application.persistentDataPath;
		path = path.Substring(0, path.LastIndexOf( '/' ) ); 
		path += "/Note_Data/";
		return Path.Combine (path, filename);
	}
	RhythmData temp = new RhythmData();
	public List<RhythmData> readStringFromFile(string filename){
		RhythmList.Clear();
		string path = pathForDocumentsFile (filename);
		print ("exist "+File.Exists(path));
		print ("path : "+path);
		if(File.Exists(path)){
			Debug.Log(path);
			FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
			StreamReader sr = new StreamReader(file);
			string str = "";
			int count=0;
			string t_str = "";
			while((t_str = sr.ReadLine())!=null){
				str+=t_str;
			}
			print (str);
			Debug.Log(str);
			string[] split_rhythm = str.Split('\\');
			for(int i=0; i<split_rhythm.Length; i++){
				print ("split : " +split_rhythm[i]);
				if(split_rhythm[i].Length==0)break;
				if(i%2==0){
					temp.RhythmTime = float.Parse(split_rhythm[i]);
				}else{
					temp.Defection = int.Parse(split_rhythm[i]);
					RhythmList.Add(new RhythmData(temp.RhythmTime,temp.Defection, 0));
					temp.Clear();
				}
			}
		}
		return RhythmList;
	}
	public void WriteRhythmDataToFile(string filename, List<RhythmData> RhythmList){
		this.RhythmList = RhythmList;
		string path = pathForDocumentsFile (filename);
		insertion_sort ();
		print ("path" + path);
		string rhythmdata="";
		for(int i=0; i<RhythmList.Count; i++){
			rhythmdata+=RhythmList[i].RhythmTime+"\\";
			rhythmdata+=RhythmList[i].Defection+"\\";
		}
		FileStream file = new FileStream (path, FileMode.Create, FileAccess.Write);
		StreamWriter sw = new StreamWriter (file);
		sw.WriteLine(rhythmdata);
		
		//sw.WriteLine (str);
		print ("writed");
		sw.Close ();
		file.Close ();
	}
	void insertion_sort(){
		int i, j ;
		float time;
		int derection;
		for(i=1; i<RhythmList.Count; i++){
			time = RhythmList[i].RhythmTime;
			derection = RhythmList[i].Defection;
			
			for(j=i-1; j>=0 && RhythmList[j].RhythmTime>time; j--){
				RhythmList[j+1].RhythmTime = RhythmList[j].RhythmTime;
				RhythmList[j+1].key = RhythmList[j].key;
				RhythmList[j+1].Defection = RhythmList[j].Defection;
			}
			RhythmList[j+1].RhythmTime = time;
			RhythmList[j+1].Defection = derection;
		}
		for(i=1; i<RhythmList.Count; i++){
			print ("after   "+ RhythmList[i].Defection);
		}
	}
}
