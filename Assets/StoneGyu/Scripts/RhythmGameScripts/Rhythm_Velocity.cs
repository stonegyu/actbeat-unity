﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rhythm_Velocity : MonoBehaviour
{
	public GameObject[] RhythmDirection_Effect = new GameObject[4];
	float falling_time = 1f; //2f
	//float currentTime=0;
	public Vector3 decideArea;
	public int thisDirection;
	int LEFT = 0, RIGHT = 1, DOWN = 2, SHAKING = 3;
	bool flag = false;
	bool score_flag = false;
	bool center_flag = false;
	
	bool Shake_flag = false;
	bool OkShake_flag = false;
	bool ShakeScorePrint_flag=false;
	bool Shake_score = false;
	bool Shakescore_flag=true;
	
	bool TotalScore_Flag = true;
	
	//int[] ML_DATA = new int[5];    // TEST CASE
	//int ML_SUM=0;               // 억셉 판정 합.
	//int ml_cnt=0;               // 억셉 갯수 카운트.
	//
	
	float x_now=0;
	float x_prev=0;
	
	float y_now=0;
	float y_prev=0;
	
	float z_now=0;
	float z_prev=0;
	//
	float a=0.3f; //LPF
	//
	public float decide_y=0;
	bool tx_flag=true;
	//
	Gyroscope gyro;
	//
	float variable;
	//노트 생성할 때마다 초기화 시켜줘야함
	//Creat_Rhythm.cs에서
	public Vector3 max_vector=Vector3.zero;
	public Vector3 min_vector=Vector3.zero;
	float maxpoint;
	
	//time for shake
	float time = 0;
	float Shake_period = 0.3f;
	
	//for sound & vibration
	bool sound_flag = true;
	bool vibration_flag = true;
	float[] decidearea = new float[4];
	List<float> Gyro_TestCase = new List<float> ();
	//float max_z_time;
	Create_Rhythm Create_rhythm;
	bool viberatorMode = true;
	void Start()
	{

		isEffected=false;
		gyro = Input.gyro;   
		gyro.enabled = true;
		thisDirection = -1;
		decideArea = GameObject.Find ("Determining_Area").transform.position;
		
		decide_y = decideArea.y;
		falling_time = Camera.main.GetComponent<Receive_RhythmData>().getFallingTime();
		viberatorMode = Camera.main.GetComponent<Receive_RhythmData>().getViberatorMode();

		if(falling_time == 2f){
			decidearea[0] = 0.1f; //perfectarea
			decidearea[1] = 0.25f; //greatearea
			decidearea[2] = 0.4f; // goodarea
			decidearea[3] = 0.5f; // bad

		}else if(falling_time == 1.5f){
			decidearea[0] = 0.15f; //perfectarea
			decidearea[1] = 0.3f; //greatearea
			decidearea[2] = 0.45f; // goodarea
			decidearea[3] = 0.6f; //
		}else if(falling_time == 1f){
			decidearea[0] = 0.2f; //perfectarea
			decidearea[1] = 0.4f; //greatearea
			decidearea[2] = 0.6f; //goodarea
			decidearea[3] = 0.8f; // bad
		}

		float velocity = 9f / falling_time;
		gameObject.rigidbody.velocity = new Vector3 (0, -velocity, 0);
		Create_rhythm = GameObject.Find ("Rhythm_Creater").transform.GetComponent<Create_Rhythm> ();

	}
	
	public float fabs(float data)   //절대값
	{
		return (data < 0) ? data * -1 : data;
	}
	bool isEffected = false;
	void Update()
	{
		variable = GameObject.Find("Rhythm_Creater").transform.GetComponent<Create_Rhythm>().exVariable;
		PointAct SensorData = new PointAct (Input.gyro.rotationRate.x, Input.gyro.rotationRate.y, Input.gyro.rotationRate.z);
		
		x_prev = x_now;
		y_prev = y_now;
		z_prev = z_now;
		
		x_now = SensorData.x;
		y_now = SensorData.y;
		z_now = SensorData.z;
		
		x_now = (a * x_prev) + ((1 - a) * x_now);   //LPF 공식.
		y_now = (a * y_prev) + ((1 - a) * y_now);   //LPF 공식.
		z_now = (a * z_prev) + ((1 - a) * z_now);   //LPF 공식.
		
		if(thisDirection!=SHAKING)
		{
			
			if(fabs(gameObject.transform.position.y-decide_y)<=decidearea[3]) //바꾸고
			{
				//키음딜레이때문에,ㅠㅠ
				if(sound_flag)
				{
					//sound_flag=false;
					//GameObject.Find("Main Camera").transform.GetComponent<EffectManager>().EffectSoundPlay();
					
				}
				if(thisDirection==LEFT)//z축 + 값
				{
					
					max_vector.z = 3f;//최소임계치
					if( max_vector.z < z_now )
					{
						gameObject.renderer.enabled = false;
						if(!isEffected){
							isEffected=true;
							Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
							GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(LEFT+1);
							
							
						}
						//if(sound_flag)
						//{
						//   sound_flag=false;
						//   GameObject.Find("Main Camera").transform.GetComponent<EffectManager>().EffectSoundPlay();
						//}
						max_vector.z = z_now;
						maxpoint = gameObject.transform.position.y;
					}
				}
				else if(thisDirection==RIGHT)//z축 - 값
				{
					
					min_vector.z = -3f;//최소임계치
					if( min_vector.z > z_now )
					{
						gameObject.renderer.enabled = false;
						if(!isEffected){
							isEffected=true;
							Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
							GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(RIGHT+1);
						}
						//if(sound_flag)
						//{
						//   sound_flag=false;
						//   GameObject.Find("Main Camera").transform.GetComponent<EffectManager>().EffectSoundPlay();
						//}
						min_vector.z = z_now;
						maxpoint = gameObject.transform.position.y;
					}
				}
				else if(thisDirection==DOWN)//x축 - 값
				{
					
					min_vector.x = -3f;//최소임계치
					if( min_vector.x > x_now )
					{
						gameObject.renderer.enabled = false;
						if(!isEffected){
							isEffected=true;
							Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
							GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(DOWN+1);
						}
						//if(sound_flag)
						//{
						//   sound_flag=false;
						//   GameObject.Find("Main Camera").transform.GetComponent<EffectManager>().EffectSoundPlay();
						//}
						min_vector.x = x_now;
						maxpoint = gameObject.transform.position.y;
					}
				}
				//else if(thisDirection==SHAKING)//
				//{
				//}
				
				//소리를넣어볼까
				
				flag=true;
			}
		}
		
		//Start of shaking
		if (thisDirection == SHAKING)
		{
			if( fabs( gameObject.transform.position.y-decide_y ) <= gameObject.transform.localScale.y/2 ) // 바꾸고
			{
				Shake_flag=true;
			}
			else
			{
				Shake_flag=false;
			}
		}else{
			Shake_flag=false;
		}

		if (Shake_flag){
			//time += Time.deltaTime;
			OkShake_flag=false;
			if(Input.gyro.rotationRate.z>=2f || Input.gyro.rotationRate.z<=-2f || Input.gyro.rotationRate.x<=-2f){
				OkShake_flag=true;
			}
			//
			if(OkShake_flag)
			{
				if(Shakescore_flag)
				{
					//perfect++
					Create_rhythm.PerfectCnt++;
					Create_rhythm.Combo++;
					if( Create_rhythm.life < 50)
					{

						Create_rhythm.life++;
					}

					Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(0);
					Shakescore_flag=false;
				}
				//scale bozong
				//perfect
				//effect

				GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(thisDirection+1);
				Vector3 scale = gameObject.transform.localScale;
				float d = fabs(gameObject.transform.position.y-GameObject.Find("Determining_Area").transform.position.y);
				float r = gameObject.transform.localScale.y;
				float new_r=(d+r/2f);

				gameObject.transform.localScale = new Vector3(scale.x, new_r, scale.z);
				Vector3 position = gameObject.transform.position;
				gameObject.transform.position = new Vector3(position.x, GameObject.Find("Determining_Area").transform.position.y+new_r/2f,position.z);
				time=0;
				if(new_r<=0.5f) {
					gameObject.transform.position = new Vector3(0, 100000, 0);
					gameObject.renderer.enabled = false;
					thisDirection=-1;
				}
			}
			else
			{
				time += Time.deltaTime;
				Shakescore_flag=true;
				if(time>Shake_period)
				{
					//miss
					vibration_flag=true;
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(3);
					Create_rhythm.MissCnt++;
					Create_rhythm.life-=2;
					time=0;
					
				}
			}

		}

		/*
		if (Shake_flag)
		{
			time += Time.deltaTime;
			if(z_now>=2f || z_now<=-2f || x_now<=-2f)
			{
				OkShake_flag=true;
			}
			if(time>Shake_period)//0.3
			{
				if(OkShake_flag)//0.3초이내 흔들었다
				{
					Vector3 scale = gameObject.transform.localScale;
					float d = fabs(gameObject.transform.position.y-GameObject.Find("Determining_Area").transform.position.y);
					float r = gameObject.transform.localScale.y;
					float new_r=(d+r/2f);
					if(new_r<=0.1f) {
						gameObject.transform.position = new Vector3(0, 100000, 0);
						gameObject.renderer.enabled = false;
					}
					gameObject.transform.localScale = new Vector3(scale.x, new_r, scale.z);
					Vector3 position = gameObject.transform.position;
					gameObject.transform.position = new Vector3(position.x, GameObject.Find("Determining_Area").transform.position.y+new_r/2f,position.z);
					Shake_score=true;
					Create_rhythm.PerfectCnt++;
					if( Create_rhythm.life < 15 )
					{
						Create_rhythm.life++;
					}
					Instantiate(RhythmDirection_Effect[thisDirection], GameObject.Find("Determining_Area").transform.position,GameObject.Find("Determining_Area").transform.rotation);
					GameObject.Find("Determining_Area").GetComponent<Change_Determining>().ChangeMaterial(SHAKING+1);

				}
				else//0.3초이내 안흔들었다
				{
					vibration_flag=true;//miss 와 함께 진동
					Shake_score=false;
					Create_rhythm.MissCnt++;
					Create_rhythm.life-=2;
				}
				ShakeScorePrint_flag=true;
				time=0;
				OkShake_flag=false;
			}
		}
		else
		{
			ShakeScorePrint_flag=false;
			Shake_score=false;
			flag=false;
		}//End of shaking
		*/
		
		
		if (((gameObject.transform.position.y - decideArea.y) < -1.0f) && ((gameObject.transform.position.y - decideArea.y) > -1.7f)) 
		{
			score_flag=true;
		}
		else 
		{
			score_flag=false;
		}
		
		if ( fabs(gameObject.transform.position.y - decideArea.y) <= 0.1f) 
		{
			center_flag=true;
		} 
		else
		{
			center_flag=false;
		}
		if(score_flag)
		{
			if(thisDirection!=SHAKING)
			{
				if( fabs(maxpoint-decide_y) <= decidearea[0] )
				{
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(0);
					if(TotalScore_Flag == true)
					{
						Create_rhythm.Combo++;
						Create_rhythm.PerfectCnt++;
						Create_rhythm.TotalScoreUpdate(500);
						TotalScore_Flag=false;
						if( Create_rhythm.life < 50 )
						{
							Create_rhythm.life+=2;
						}
					}
				}
				else if(fabs(maxpoint-decide_y) <= decidearea[1] )
				{
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(1);
					if(TotalScore_Flag == true)
					{
						Create_rhythm.Combo++;
						Create_rhythm.GreateCnt++;
						TotalScore_Flag=false;
						Create_rhythm.TotalScoreUpdate(300);
						if( Create_rhythm.life < 50 )
						{
							Create_rhythm.life++;
						}
					}
				}
				else if(fabs(maxpoint-decide_y) <= decidearea[2])
				{
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(2);
					if(TotalScore_Flag == true)
					{

						Create_rhythm.Combo=0;
						Create_rhythm.GoodCnt++;
						TotalScore_Flag=false;
						Create_rhythm.TotalScoreUpdate(50);
					}
				}
				else if(fabs(maxpoint-decide_y) <= decidearea[3])
				{
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(3);
					if(TotalScore_Flag == true)
					{

						Create_rhythm.Combo=0;
						Create_rhythm.BadCnt++;
						TotalScore_Flag=false;
						Create_rhythm.life--;
						Create_rhythm.TotalScoreUpdate(10);
					}
				}
				else if(maxpoint==0)
				{
					if(vibration_flag && viberatorMode)//진동을 준다
					{
						//vibration;
						Handheld.Vibrate();
						vibration_flag=false;//한번만
					}
					GameObject.Find("GUIManager").GetComponent<GUIController>().DisplayDecideTexture(4);
					if(TotalScore_Flag == true)
					{
						Create_rhythm.Combo=0;
						Create_rhythm.MissCnt++;
						TotalScore_Flag=false;
						Create_rhythm.life-=2;
					}
				}
			}      
		}
		

		/*
		if (ShakeScorePrint_flag)
		{
			
			if(Shake_score)
			{

			}
			else
			{
				if(vibration_flag)//진동을 준다
				{
					//vibration;
					Handheld.Vibrate();
					vibration_flag=false;//한번만
				}

			}
		}*/
		
		if(center_flag)
		{

		}
		
		if(score_flag && !Create_rhythm.TestCaseIsFull())
		{
			if(maxpoint!=0f && tx_flag==true)
			{
				if(!Create_rhythm.TestCaseIsFull()){
					Create_rhythm.Call_TestCase(maxpoint);
				}
				tx_flag=false;
			}
		}
		//InitLine
		if ((gameObject.transform.position.y - decideArea.y) < -(gameObject.transform.localScale.y/2+3.0f))
		{
			isEffected=false;
			gameObject.transform.position = new Vector3(transform.position.x, 100000f, transform.position.z);
			max_vector=Vector3.zero;
			min_vector=Vector3.zero;
			maxpoint=0;
			x_now=0;
			y_now=0;
			z_now=0;
			tx_flag=true;
			TotalScore_Flag = true;
			sound_flag=true;
			vibration_flag=true;
		}
	}
}

public class PointAct
{
	public float x;
	public float y;
	public float z;
	public PointAct(float x, float y, float z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}