﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Create_Rhythm : MonoBehaviour {
	public Material[] Rhythm_Material;
	public GameObject Prefab_Obj;
	GameObject[] Recycle_Obj = new GameObject[10];	//game object 10
	int CurrentIndex = 0;
	Vector3 temp;

	Vector3 note_format;

	//*외부변수지정
	//public int[] DecideLine = new int[5]; // TEST CASE
	public ArrayList bozong = new ArrayList();

	public bool input_testcase(Vector3 input_gyro){
		if(!testcase_isfull()){
			bozong.Add(input_gyro);
			return true;
		}
		return false;
	}

	public bool testcase_isfull()
	{
		return bozong.Count == 5;
	}

	public float exVariable = 3;
	public float[] ML_DATA = new float[5];	//no
	public int ML_cnt=0;//no
	public float ML_SUM=0;//no
	public float decide_y=0;//no

	//학습 판정용
	public bool Complete_DecideArea = false;	//decide area
	public float DecideArea = 0;		
	List<float> Gyro_TestCase = new List<float>();

	//점수환산용
	public int PerfectCnt=0; 	//500
	public int GreateCnt=0;		//100
	public int GoodCnt=0;		//10
	public int BadCnt =0;
	public int MissCnt=0;		//
	public int TotalScore=0;
	public int Combo = 0;

	//라이프
	public int life;


	void Start ()
	{

	}
	public void CreateRhythmStart(){
		int CurrentIndex = 0;
		life = 50;
		for(int i=0; i<Recycle_Obj.Length; i++)
		{

			Recycle_Obj[i] = (GameObject) Instantiate(Prefab_Obj);
			Recycle_Obj[i].name = "Recycle_Obj"+i;
			Recycle_Obj[i].transform.position = new Vector3(0, 10000f, 0);
			Recycle_Obj[i].renderer.enabled = false;
		}
		note_format = Recycle_Obj [0].transform.localScale;
	}
	//
	void OnGUI()
	{
		GUI.skin.label.fontSize = Screen.width/10;
		GUI.color = Color.black;
		//GUI.Label(new Rect(0, Screen.height/2, Screen.width, Screen.height/7),"decideY : "+DecideArea+"\ncount"+Gyro_TestCase.Count);
	}
	//
	public void TotalScoreUpdate(int inputScore){
		TotalScore+= inputScore+(Combo*10);
	}
	void Update()
	{
		GameObject.Find("GUIManager").GetComponent<GUIController>().PutTotalScore(TotalScore);
		GameObject.Find("GUIManager").GetComponent<GUIController>().SetEnergy(life);
		if(TestCaseIsFull() && !Complete_DecideArea)
		{
			DecideArea = Resize_DecideArea();
			for(int i=0; i<Recycle_Obj.Length; i++)
			{
				Recycle_Obj[i].transform.GetComponent<Rhythm_Velocity>().decide_y = DecideArea;
			}
			Complete_DecideArea = true;
		}
	}

	//testcase가 5개 일때 , 
	public bool TestCaseIsFull()
	{
		return Gyro_TestCase.Count == 5;
	}

	//DecideArea 재조정. 
	public float Resize_DecideArea(){
		float sum = 0;
		for(int i=0; i<Gyro_TestCase.Count; i++){
			sum+=Gyro_TestCase[i];
		}
		return sum / Gyro_TestCase.Count;
	}

	//
	public void Call_TestCase(float Case)
	{
		Gyro_TestCase.Add (Case);
	}


	//노트생성 및 방향 정보 [shake기능까지.]
	public void Create_RhythmObj(int Derection, bool IsShaking, float Duration)
	{
		//노트 생성될떄 , 
		Recycle_Obj[CurrentIndex%Recycle_Obj.Length].renderer.enabled = true;
		Recycle_Obj[CurrentIndex%Recycle_Obj.Length].renderer.material = Rhythm_Material[Derection];
		Recycle_Obj[CurrentIndex%Recycle_Obj.Length].GetComponent<Rhythm_Velocity> ().thisDirection = Derection; //방향데이터 임포트 시켜줌
		if(IsShaking)
		{
			Recycle_Obj[CurrentIndex%Recycle_Obj.Length].gameObject.transform.position = gameObject.transform.position;
			temp = gameObject.transform.position;
			Recycle_Obj[CurrentIndex%Recycle_Obj.Length].gameObject.transform.position = new Vector3(temp.x,temp.y+(Duration*4.5f)/2f,temp.z);
			temp = note_format;
			Recycle_Obj[CurrentIndex++%Recycle_Obj.Length].gameObject.transform.localScale = new Vector3(temp.x, temp.y+temp.y*(Duration*4.5f), temp.z); 
		}
		else
		{
			Recycle_Obj[CurrentIndex%Recycle_Obj.Length].gameObject.transform.localScale = note_format;
			Recycle_Obj[CurrentIndex++%Recycle_Obj.Length].gameObject.transform.position = gameObject.transform.position;
		}

	}
}
