﻿using UnityEngine;
using System.Collections;

public class EffectManager : MonoBehaviour {

	public AudioClip BitSound;
	public AudioSource EffectAudiosource;

	//
	public void EffectSoundPlay()
	{
		EffectAudiosource.clip = BitSound;
		EffectAudiosource.playOnAwake = true;
		EffectAudiosource.Play();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
