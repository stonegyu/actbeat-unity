﻿using UnityEngine;
using System.Collections;

public class EffectObjDestroy : MonoBehaviour {
	float Lifetime = 0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Lifetime += Time.deltaTime;
		if(Lifetime>0.5f){
			Destroy(gameObject);
		}
	}
}
