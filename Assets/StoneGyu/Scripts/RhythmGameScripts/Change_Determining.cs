﻿using UnityEngine;
using System.Collections;

public class Change_Determining : MonoBehaviour {
	public Material[] Determining_Material = new Material[4];
	int CurrentColor = 0;
	bool isChanged = false;
	float ChangedTime=0;
	public void ChangeMaterial(int Direction){
		gameObject.renderer.material = Determining_Material[Direction];
		CurrentColor=Direction;
		isChanged = true;
		ChangedTime=0;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(isChanged){
			ChangedTime+=Time.deltaTime;
			if(ChangedTime>0.3f){
				ChangedTime=0;
				isChanged = false;
				ChangeMaterial(0);
			}
		}
	}
}
