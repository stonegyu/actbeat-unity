﻿using UnityEngine;
using System.Collections;

public class Splash : MonoBehaviour {
	AsyncOperation async;
	string sceneName = "";
	public void CallAndroidFunction(string str){
		AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
		jo.Call ("AndroidFunction", str);
	}
	public void Load_Scene(string scenename){
		this.sceneName = scenename;
	}
	IEnumerator Start(){
		print ("sceneload");
		CallAndroidFunction("sceneload");
		while(true){
			if(sceneName.Length == 0){
				yield return new WaitForSeconds(0.01f);
			}else{
				break;
			}
			print ("sceneName : "+sceneName);
		}
		Application.LoadLevel(sceneName);
	}
}
